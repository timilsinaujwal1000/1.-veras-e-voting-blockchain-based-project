import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAnimations } from '@fuse/animations';
import { TranslateService } from '@ngx-translate/core';
import { AppRoutes } from 'app/AppRoutes';
import { ContractStatus, ContractStatuses } from 'app/contractStatus';
import { DownloadTypes } from 'app/downloadTypes';

import { TabStatus } from 'app/modules/constantsEnum';
import { Pagination } from 'app/pagination';
import { AuthUSerService } from 'app/services/authUserService';
import { CustomerService } from 'app/services/customer/customer.service';
import { TranslationService } from 'app/services/translationService';
import { UtilitiesService } from 'app/services/utilitiesService';
import { Observable, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-manage-identity',
  templateUrl: './manage-identity.component.html',
  styleUrls: ['./manage-identity.component.scss']
})
export class ManageIdentityComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
